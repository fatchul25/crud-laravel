@extends('halaman.index')

@section('content')
<div class="ml-3 mt-3">
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Edit Cast {{$post->id}}</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/cast" method="POST">
        @csrf
      <div class="card-body">
        <div class="form-group">
          <label for="nama">Nama</label>
          <input type="text" class="form-control" id="nama" name="nama" value="{{ old('nama', $post->nama) }}" placeholder="nama" required>
            @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
          <label for="umur">Umur</label>
          <input type="text" class="form-control" id="umur" name="umur" value="{{ old('umur', $post->umur) }}" placeholder="umur" required>
          @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
        <div class="form-group">
            <label for="umur">Bio</label>
            <input type="text" class="form-control" id="bio" name="bio" value="{{ old('bio', $post->bio) }}" placeholder="bio" required>
            @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
          </div>  
      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Create</button>
      </div>
    </form>
  </div>
</div>
@endsection