@extends('halaman.index')

@section('content')
    <div class="mt-3 ml-3">
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">Cast Table</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                @if(session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                <a class="btn btn-primary mb-2" href="/cast/create"> Create New Cast </a>
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>Nama</th>
                    <th>Umur</th>
                    <th>Bio</th>
                    <th style="width: 40px">Action</th>
                  </tr>
                </thead>
                <tbody>
                    @forelse($cast as $key => $post)
                        <tr>
                            <td> {{ $key + 1 }} </td>
                            <td> {{ $post -> nama }} </td>
                            <td> {{ $post -> umur }} </td>
                            <td> {{ $post -> bio }} </td>
                            <td style="display: flex;">   
                                <a href="/cast/{{ $post -> id }}" class="btn btn-info btn-sm"> show </a>
                                <a href="/cast/{{ $post -> id }}/edit" class="btn btn-default btn-sm"> edit </a>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="4" align="center"> No Post </td>
                        </tr>
                    @endforelse
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
            {{-- <div class="card-footer clearfix">
              <ul class="pagination pagination-sm m-0 float-right">
                <li class="page-item"><a class="page-link" href="#">«</a></li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#">»</a></li>
              </ul>
            </div>--}}
          </div> 
    </div>
@endsection