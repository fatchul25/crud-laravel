<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function create() {
        return view('cast.create');
    }

    public function store(Request $request) {
        //dd($request->all());
        $request->validate([
            'nama' => 'required|unique:cast',
            'umur' => 'required|unique:cast',
            'bio' => 'required'
        ]);

        $query = DB::table('cast')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);

        return redirect('/cast/{cast_id}')->with('success', 'Post Berhasil Disimpan!');
    }

    public function show() {
        $cast = DB::table('cast')->get();
        //dd($cast);
        return view('cast.show', compact('cast'));
    }

    public function show2($id) {
        $post = DB::table('cast')->where('id', $id)->first();
        //dd($post);
        return view('cast.show2', compact('post'));
    }

    public function edit($id) {
        $post = DB::table('cast')->where('id', $id)->first();
        return view('cast.edit', compact('post'));
    }
}
