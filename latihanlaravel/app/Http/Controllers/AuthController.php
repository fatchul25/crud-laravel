<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('halaman.register');
    }

    public function welcome(Request $request)
    {
        // dd($request->all());
        $namadepan = $request['namadepan'];
        $namabelakang = $request['namabelakang'];
        $jenis_kelamin = $request['jenis_kelamin'];
        $area = $request['area'];
        $nationality = $request['nationality'];
        $bahasa = $request['bahasa'];
        $bio = $request['bio'];

        return view('halaman.welcome', compact('namadepan', 'namabelakang', 'jenis_kelamin', 'nationality', 'area', 'bahasa', 'bio'));
    }
}
